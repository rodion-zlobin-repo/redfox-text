# Test job for Redfox

By Rodion Zlobin (<rodion.zlobin@gmail.com>)

## Link to the site

[https://143903.selcdn.ru/Redfox/index.html](https://143903.selcdn.ru/Redfox/index.html)

## Install

```
npm i
```

## Run development

```
npm run dev
```

## Build site

```
npm run build
```

## FYI

Responsive branch is a WIP branch for responsive layout
